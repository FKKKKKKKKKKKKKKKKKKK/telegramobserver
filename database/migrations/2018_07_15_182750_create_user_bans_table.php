<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBansTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('user_bans', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->bigInteger('chat_id');
            $table->boolean('is_done')->default(false);
            $table->string('token');
            $table->string('forgive_token');
            $table->string('kick_token');
            $table->bigInteger('banned_by_id')->nullable();
            $table->bigInteger('forgiven_by_id')->nullable();
            $table->bigInteger('report_message_id')->nullable();
            $table->bigInteger('report_chat_id')->nullable();
            $table->timestamps();
            $table->index('token', 'user_bans_token_index', 'hash');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('user_bans');
    }
}
