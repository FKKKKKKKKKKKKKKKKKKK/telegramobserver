<?php

namespace App\Console\Commands;

use App\Bot\UpdateHandler;
use Illuminate\Console\Command;
use Longman\TelegramBot\Request;
use PhpTelegramBot\Laravel\PhpTelegramBot;
use PhpTelegramBot\Laravel\PhpTelegramBotContract;

class RunBot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bot:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client([
            'base_uri' => 'https://api.telegram.org',
      //      'proxy' => 'socks5h://192.168.99.1:1080'
        ]);
        Request::setClient($client);
        /** @var PhpTelegramBot $bot */
        $bot = app(PhpTelegramBotContract::class);

        $handler = app(UpdateHandler::class);

        while (true) {
            try {
                $result = $bot->handleGetUpdates(20, 5);
            } catch (\Throwable $e) {
                logger()->error('tg fail', ['exception' => $e]);
                sleep(5);
                continue;
            }
            foreach ($result->getResult() as $update) {
                try {
                    $handler->handleUpdate($update);
                } catch (\Throwable $e) {
                    logger()->error('Error', [
                        'exception' => $e,
                    ]);
                }
            }
            logger()->debug('next round');
        }
    }
}
