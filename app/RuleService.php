<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 15.07.2018
 * Time: 19:10.
 */

namespace App;

use App\Models\Chat;
use App\Rules\Actions\BanAddedBots;
use App\Rules\Actions\BanFromChat;
use App\Rules\Actions\DispatchCheckForInactivity;
use App\Rules\Actions\ReportBan;
use App\Rules\Actions\ReportMessage;
use App\Rules\Conditions\ContainsRTL;
use App\Rules\Conditions\FirstNMessages;
use App\Rules\Conditions\IsSpammy;
use App\Rules\Conditions\MessageContainsLink;
use App\Rules\Conditions\MessageContainsMention;
use App\Rules\Conditions\MessageIsForwarded;
use App\Rules\Conditions\NotAdminAddsBot;
use App\Rules\Conditions\UserJoinedChat;
use App\Rules\Rule;

class RuleService
{
    public function getChatRules(Chat $chat)
    {
        $banAndReport = [new BanFromChat(), new ReportBan()];

        return collect([
//            new Rule([new FirstNMessages(100000)], [new ReportMessage()]),
            new Rule([new FirstNMessages(10), new MessageContainsMention()],
                [new ReportMessage()]),
            new Rule([new FirstNMessages(10), new MessageIsForwarded()],
                [new ReportMessage()]),
            new Rule([new FirstNMessages(100), new MessageContainsLink(MessageContainsLink::SOURCE_BLACKLIST)],
                $banAndReport),
            new Rule([new FirstNMessages(10), new MessageContainsLink(
                MessageContainsLink::SOURCE_BLACKLIST | MessageContainsLink::SOURCE_GREY_ZONE)],
                [new ReportMessage()]),
            new Rule([new UserJoinedChat()], [new DispatchCheckForInactivity()]),
            new Rule([new FirstNMessages(10), new ContainsRTL()], [new ReportMessage()]),
            new Rule([new IsSpammy(10, 5)], [new ReportMessage()]),
            new Rule([new NotAdminAddsBot()], [new BanAddedBots(), new ReportMessage()]),
        ]);
    }
}
