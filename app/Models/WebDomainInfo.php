<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WebDomainInfo.
 *
 * @property int                 $id
 * @property string              $domain
 * @property string              $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebDomainInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebDomainInfo whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebDomainInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebDomainInfo whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebDomainInfo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class WebDomainInfo extends Model
{
}
