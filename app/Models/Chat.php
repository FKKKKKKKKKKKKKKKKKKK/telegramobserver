<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Chat.
 *
 * @property int                   $id                             Unique user or chat identifier
 * @property string                $type                           Chat type, either private, group, supergroup or channel
 * @property string|null           $title                          Chat (group) title, is null if chat type is private
 * @property string|null           $username                       Username, for private chats, supergroups and channels if available
 * @property int|null              $all_members_are_administrators True if a all members of this group are admins
 * @property \Carbon\Carbon|null   $created_at
 * @property \Carbon\Carbon|null   $updated_at
 * @property int|null              $old_id                         Unique chat identifier, this is filled when a group is converted to a supergroup
 * @property int|null              $moderation_chat_id
 * @property int                   $is_bot_enabled
 * @property \App\Models\Chat|null $moderationChat
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereAllMembersAreAdministrators($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereIsBotEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereModerationChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereOldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chat whereUsername($value)
 * @mixin \Eloquent
 */
class Chat extends Model
{
    protected $table = 'chat';

    public function moderationChat()
    {
        return $this->belongsTo(Chat::class, 'moderation_chat_id', 'id');
    }
}
