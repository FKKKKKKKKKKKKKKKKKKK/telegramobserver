<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserBan.
 *
 * @property int                 $id
 * @property int                 $user_id
 * @property int                 $chat_id
 * @property int                 $is_done
 * @property string              $token
 * @property string              $forgive_token
 * @property int|null            $banned_by_id
 * @property int|null            $forgiven_by_id
 * @property int|null            $report_message_id
 * @property int|null            $report_chat_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \App\Models\Chat    $chat
 * @property \App\Models\User    $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereBannedById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereForgiveToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereForgivenById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereIsDone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereReportChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereReportMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBan whereUserId($value)
 * @mixin \Eloquent
 */
class UserBan extends Model
{
    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function boot()
    {
        parent::boot();
        static::unguard();

        static::creating(function (UserBan $ban) {
            $ban->is_done = false;
            $ban->token = 'ban'.str_random(60);
            $ban->forgive_token = 'forgive'.str_random(45);
            $ban->kick_token = 'kick'.str_random(45);
        });
    }
}
