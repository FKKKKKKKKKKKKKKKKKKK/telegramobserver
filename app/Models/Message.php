<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Models\Message.
 *
 * @property int                   $chat_id                 Unique chat identifier
 * @property int                   $id                      Unique message identifier
 * @property int|null              $user_id                 Unique user identifier
 * @property string|null           $date                    Date the message was sent in timestamp format
 * @property int|null              $forward_from            Unique user identifier, sender of the original message
 * @property int|null              $forward_from_chat       Unique chat identifier, chat the original message belongs to
 * @property int|null              $forward_from_message_id Unique chat identifier of the original message in the channel
 * @property string|null           $forward_date            date the original message was sent in timestamp format
 * @property int|null              $reply_to_chat           Unique chat identifier
 * @property int|null              $reply_to_message        Message that this message is reply to
 * @property string|null           $media_group_id          The unique identifier of a media message group this message belongs to
 * @property string|null           $text                    For text messages, the actual UTF-8 text of the message max message length 4096 char utf8mb4
 * @property string|null           $entities                For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
 * @property string|null           $audio                   Audio object. Message is an audio file, information about the file
 * @property string|null           $document                Document object. Message is a general file, information about the file
 * @property string|null           $photo                   Array of PhotoSize objects. Message is a photo, available sizes of the photo
 * @property string|null           $sticker                 Sticker object. Message is a sticker, information about the sticker
 * @property string|null           $video                   Video object. Message is a video, information about the video
 * @property string|null           $voice                   Voice Object. Message is a Voice, information about the Voice
 * @property string|null           $video_note              VoiceNote Object. Message is a Video Note, information about the Video Note
 * @property string|null           $contact                 Contact object. Message is a shared contact, information about the contact
 * @property string|null           $location                Location object. Message is a shared location, information about the location
 * @property string|null           $venue                   Venue object. Message is a Venue, information about the Venue
 * @property string|null           $caption                 For message with caption, the actual UTF-8 text of the caption
 * @property string|null           $new_chat_members        List of unique user identifiers, new member(s) were added to the group, information about them (one of these members may be the bot itself)
 * @property int|null              $left_chat_member        Unique user identifier, a member was removed from the group, information about them (this member may be the bot itself)
 * @property string|null           $new_chat_title          A chat title was changed to this value
 * @property string|null           $new_chat_photo          Array of PhotoSize objects. A chat photo was change to this value
 * @property int|null              $delete_chat_photo       Informs that the chat photo was deleted
 * @property int|null              $group_chat_created      Informs that the group has been created
 * @property int|null              $supergroup_chat_created Informs that the supergroup has been created
 * @property int|null              $channel_chat_created    Informs that the channel chat has been created
 * @property int|null              $migrate_to_chat_id      Migrate to chat identifier. The group has been migrated to a supergroup with the specified identifier
 * @property int|null              $migrate_from_chat_id    Migrate from chat identifier. The supergroup has been migrated from a group with the specified identifier
 * @property string|null           $pinned_message          Message object. Specified message was pinned
 * @property string|null           $connected_website       The domain name of the website on which the user has logged in.
 * @property int                   $is_reported
 * @property \Carbon\Carbon|null   $created_at
 * @property \Carbon\Carbon|null   $updated_at
 * @property \App\Models\Chat      $chat
 * @property \App\Models\User|null $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereAudio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCaption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereChannelChatCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereConnectedWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereContact($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereDeleteChatPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereDocument($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereEntities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereForwardDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereForwardFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereForwardFromChat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereForwardFromMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereGroupChatCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereIsReported($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereLeftChatMember($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMediaGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMigrateFromChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereMigrateToChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereNewChatMembers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereNewChatPhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereNewChatTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message wherePinnedMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereReplyToChat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereReplyToMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereSticker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereSupergroupChatCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereVenue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereVideo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereVideoNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Message whereVoice($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $table = 'message';

    public static function boot()
    {
        parent::boot();
        static::unguard();
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getText(int $limit = 0)
    {
        if ($this->new_chat_members) {
            $members = explode(',', $this->new_chat_members);
            $result = 'Added ';
            for ($i = 0; $i < \count($members); ++$i) {
                $result .= User::find($members[$i])->getDescription();
                if ($i !== \count($members) - 1) {
                    $result .= ',';
                }
            }

            return $result;
        }

        return Str::limit($this->text, $limit);
    }
}
