<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\User.
 *
 * @property int                 $id            Unique user identifier
 * @property int|null            $is_bot        True if this user is a bot
 * @property string              $first_name    User's first name
 * @property string|null         $last_name     User's last name
 * @property string|null         $username      User's username
 * @property string|null         $language_code User's system language
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLanguageCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Model
{
    protected $table = 'user';

    public function getDescription()
    {
        return sprintf(
            '[%s](tg://user?id=%s)',
            $this->username ?: ($this->first_name.' '.$this->last_name),
            $this->id
        );
    }
}
