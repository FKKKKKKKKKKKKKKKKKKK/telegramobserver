<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CallbackQuery.
 *
 * @property int                   $id                Unique identifier for this query
 * @property int|null              $user_id           Unique user identifier
 * @property int|null              $chat_id           Unique chat identifier
 * @property int|null              $message_id        Unique message identifier
 * @property string|null           $inline_message_id Identifier of the message sent via the bot in inline mode, that originated the query
 * @property string                $data              Data associated with the callback button
 * @property \Carbon\Carbon|null   $created_at        Entry date creation
 * @property \App\Models\User|null $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereInlineMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CallbackQuery whereUserId($value)
 * @mixin \Eloquent
 */
class CallbackQuery extends Model
{
    protected $table = 'callback_query';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
