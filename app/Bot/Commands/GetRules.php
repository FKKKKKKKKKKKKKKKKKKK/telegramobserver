<?php

namespace App\Bot\Commands;

use App\Bot\BotCommand;
use App\Models\Message;
use App\Rules\Rule;
use App\RuleService;
use Illuminate\Support\Collection;
use Longman\TelegramBot\Request;
use PhpTelegramBot\Laravel\PhpTelegramBotContract;

class GetRules implements BotCommand
{
    public function getCommandName()
    {
        return 'rules';
    }

    private function getMessage(Message $message)
    {
        /** @var Collection|Rule[] $rules */
        $rules = app(RuleService::class)->getChatRules($message->chat);

        if ($rules->isEmpty()) {
            return 'No rules set';
        }
        $result = "Rules:\n-------------------\n";
        foreach ($rules as $rule) {
            $result .= $rule->getDescription();
            $result .= "-------------------\n";
        }

        return $result;
    }

    public function handle(Message $message)
    {
        $text = $this->getMessage($message);
        /* @var PhpTelegramBotContract $bot */
        Request::sendMessage([
            'chat_id' => $message->chat_id,
            'text' => $text,
            'parse_mode' => 'markdown',
        ]);
    }
}
