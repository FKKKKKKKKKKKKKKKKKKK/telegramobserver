<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 15.07.2018
 * Time: 18:37.
 */

namespace App\Bot;

use App\CallbackQueryService;
use App\CommandService;
use App\Models\CallbackQuery;
use App\Models\Message;
use App\Rules\Rule;
use App\RuleService;
use Carbon\Carbon;
use DB;
use Longman\TelegramBot\Entities\Update;

class UpdateHandler
{
    public function handleUpdate(Update $update)
    {
        $messageEntity = $update->getMessage();
        if ($messageEntity !== null) {
            logger()->debug(var_export($messageEntity, true));

            /** @var Message|null $message */
            $message = Message::query()
                ->where('chat_id', $messageEntity->getChat()->getId())
                ->where('id', $messageEntity->getMessageId())
                ->first();

            if ($message === null) {
                return;
            }

            if ($message->new_chat_members) {
                foreach (explode(',', $message->new_chat_members) as $userId) {
                    DB::table('user_chat')
                        ->where('user_id', $userId)
                        ->where('chat_id', $message->chat_id)
                        ->update([
                            'date' => Carbon::now(),
                        ]);
                }
            }

            if (!$message->chat->is_bot_enabled) {
                return;
            }

            if ($messageEntity->getCommand()) {
                app(CommandService::class)->executeCommand($messageEntity->getCommand(), $message);
            }

            $rules = app(RuleService::class)->getChatRules($message->chat);
            /** @var Rule $rule */
            foreach ($rules as $rule) {
                $rule->process($message, collect($messageEntity->getEntities()));
            }
        } else {
            $callbackQuery = $update->getCallbackQuery();
            if ($callbackQuery !== null) {
                $model = CallbackQuery::whereId($callbackQuery->getId())->first();
                if ($model !== null) {
                    app(CallbackQueryService::class)->handle($model);
                }
            }
        }
    }
}
