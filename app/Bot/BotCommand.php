<?php

namespace App\Bot;

use App\Models\Message;

interface BotCommand
{
    public function getCommandName();

    public function handle(Message $message);
}
