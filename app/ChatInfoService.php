<?php

namespace App;

use App\Models\Chat;
use App\Models\User;
use Cache;
use Longman\TelegramBot\Request;

class ChatInfoService
{
    public function getChatAdministrators(Chat $chat)
    {
        $key = "tgb:moderators:$chat->id";
        if (!Cache::has($key)) {
            $result = Request::getChatAdministrators([
                'chat_id' => $chat->id,
            ]);
            $mods = collect($result->getResult())->pluck('user.id')->toArray();
            Cache::put($key, implode($mods, ','), 10);
        }
        $result = Cache::get($key);

        return User::query()->whereIn('id', collect(explode(',', $result)))->get();
    }

    public function isChatAdministrator(Chat $chat, User $user): bool
    {
        $admins = $this->getChatAdministrators($chat);

        return $admins->contains('id', $user->id);
    }
}
