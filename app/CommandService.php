<?php

namespace App;

use App\Bot\BotCommand;
use App\Models\Message;
use Illuminate\Support\Collection;

class CommandService
{
    /** @var Collection|BotCommand[] */
    private $commands;

    /**
     * @param BotCommand[]|Collection $commands
     *
     * @return CommandService
     */
    public function setCommands(Collection $commands)
    {
        $this->commands = $commands;

        return $this;
    }

    public function executeCommand(string $commandName, Message $message)
    {
        foreach ($this->commands as $command) {
            if ($command->getCommandName() === $commandName) {
                $command->handle($message);
            }
        }
    }
}
