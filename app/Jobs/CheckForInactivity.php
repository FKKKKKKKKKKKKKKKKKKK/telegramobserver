<?php

namespace App\Jobs;

use App\Models\Chat;
use App\Models\Message;
use App\Models\User;
use App\Models\UserBan;
use App\ReportService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Longman\TelegramBot\Entities\ServerResponse;
use PhpTelegramBot\Laravel\PhpTelegramBotContract;

class CheckForInactivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var User */
    private $user;

    /** @var Chat */
    private $chat;

    /**
     * CheckForInactivity constructor.
     *
     * @param User $user
     * @param Chat $chat
     */
    public function __construct(int $delay, User $user, Chat $chat)
    {
        if ($delay !== 0) {
            $this->delay($delay);
        }
        $this->user = $user;
        $this->chat = $chat;
    }

    /**
     * CheckForInactivity constructor.
     *
     * @param int     $delay
     * @param Message $message
     */

    /**
     * Create a new job instance.
     */

    /**
     * Execute the job.
     */
    public function handle()
    {
        app(PhpTelegramBotContract::class);
        /** @var Message|null $newerMessage */
        $newerMessage = Message::where('chat_id', $this->chat->id)
            ->where('user_id', $this->user->id)
            ->orderByDesc('id')
            ->first();

        if ($newerMessage === null || $newerMessage->text === null) {
            $moderationChat = $this->chat->moderationChat ?? $this->chat;
            $text = sprintf('User %s joined chat %s and was inactive for %s seconds',
                $this->user->getDescription(),
                $this->chat->title,
                $this->delay
                );
            $ban = UserBan::create([
                'chat_id' => $this->chat->id,
                'user_id' => $this->user->id,
            ]);

            /** @var ServerResponse $result */
            $result = app(ReportService::class)->sendReportMessage($moderationChat,
                $text,
                $ban
            );
            if ($result->isOk()) {
                $ban->report_message_id = $result->getResult()->message_id;
                $ban->report_chat_id = $result->getResult()->getChat()->id;
                $ban->save();
            }
        }
    }
}
