<?php

namespace App\Providers;

use App\CommandService;
use Illuminate\Support\ServiceProvider;

class CommandServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     */
    public function boot()
    {
    }

    /**
     * Register services.
     */
    public function register()
    {
        $this->app->singleton(CommandService::class, function (): CommandService {
            return (new CommandService())
          ->setCommands(collect([
              //  new GetRules()
          ]));
        });
    }
}
