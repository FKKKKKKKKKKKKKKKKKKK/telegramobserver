<?php

namespace App;


use App\Models\CallbackQuery;
use App\Models\User;
use App\Models\UserBan;
use Illuminate\Support\Str;
use Longman\TelegramBot\Request;

class CallbackQueryService
{
    public function handle(CallbackQuery $query)
    {
        $data = $query->data;
        if (Str::startsWith($data, ["ban", "kick"])) {

            $ban = UserBan::whereToken($data)->orWhere("kick_token", $data)->first();
            if ($ban !== null && !$ban->is_done && !$ban->forgiven_by_id) {
                if (app(ChatInfoService::class)->isChatAdministrator($ban->chat, $query->user)) {
                    $ban->banned_by_id = $query->user_id;
                    $this->banOrKick($ban, Str::startsWith($data, "kick"));
                }
            }
        } elseif (Str::startsWith($data, "forgive")) {
            $ban = UserBan::whereForgiveToken($data)->first();
            if ($ban !== null && !$ban->is_done) {
                $this->forgive($ban, $query->user);
            }
        }
    }

    private function banOrKick(UserBan $ban, ?bool $kick = false)
    {

        $user = $ban->user;
        $chat = $ban->chat;

        $modChannel = $chat->moderationChat ?? $chat;

        $result = Request::kickChatMember([
            "chat_id" => $chat->id,
            "user_id" => $user->id,
        ]);

        $moderator = User::find($ban->banned_by_id);

        if ($result->isOk()) {
            /** @noinspection PhpUnhandledExceptionInspection */
            Request::sendMessage([
                'chat_id' => $modChannel->id,
                'text' => sprintf('User %s has been %s from %s by %s ',
                    $user->getDescription(),
                    $kick ? "kicked " : "banned",
                    $chat->title,
                    $moderator->getDescription()
                ),
                'reply_to_message_id' => $ban->report_message_id,
                'parse_mode' => 'markdown',
            ]);

            if ($kick) {
                $kickResult = Request::unbanChatMember([
                    "chat_id" => $ban->chat_id,
                    "user_id" => $ban->user_id
                ]);
                dump($ban->toArray(), $kickResult);
            }

            Request::editMessageReplyMarkup([
                "chat_id" => $ban->report_chat_id,
                "message_id" => $ban->report_message_id,
                "reply_markup" => null
            ]);

            $ban->is_done = 1;
            $ban->save();
        } else {
            app(ReportService::class)->sendTextMessageToChat($modChannel,
                sprintf('%s tried to ban %s from %s. Error: %s',
                    $moderator->getDescription(),
                    $user->getDescription(),
                    $chat->title,
                    $result->getDescription()
                ));
        }

    }

    private function forgive(UserBan $ban, User $forgivenBy)
    {
        $ban->forgiven_by_id = $forgivenBy->id;
        $ban->save();

        $modChannel = $ban->chat->moderationChat ?? $ban->chat;

        /** @noinspection PhpUnhandledExceptionInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        Request::sendMessage([
            'chat_id' => $modChannel->id,
            'text' => sprintf('User %s has been forgiven by %s ',
                $ban->user->getDescription(),
                User::query()->find($ban->forgiven_by_id)->getDescription()
            ),
            'reply_to_message_id' => $ban->report_message_id,
            'parse_mode' => 'markdown',
        ]);

        Request::editMessageReplyMarkup([
            "chat_id" => $ban->report_chat_id,
            "message_id" => $ban->report_message_id,
            "reply_markup" => null
        ]);
    }
}