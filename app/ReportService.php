<?php

namespace App;

use App\Models\Chat;
use App\Models\UserBan;
use Longman\TelegramBot\Entities\InlineKeyboardButton;
use Longman\TelegramBot\Request;

class ReportService
{
    public function sendReportMessage(Chat $chat, string $text, ?UserBan $ban)
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        if ($ban) {
            $markup = [
                [
                    new InlineKeyboardButton([
                        "text" => "Ban",
                        "callback_data" => $ban->token
                    ]),
                    new InlineKeyboardButton([
                        "text" => "Kick",
                        "callback_data" => $ban->kick_token,
                    ]),
                    new InlineKeyboardButton([
                        "text" => "Forgive",
                        "callback_data" => $ban->forgive_token
                    ])
                ]
            ];
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $result = Request::sendMessage([
            "text" => $text,
            "reply_markup" => [
                'inline_keyboard' => $markup
            ],
            'parse_mode' => 'markdown',
            "chat_id" => $chat->id,
        ]);
        return $result;
    }

    public function sendTextMessageToChat(Chat $chat, string $text){
        return Request::sendMessage([
            "chat_id" => $chat->id,
            "parse_mode" => "markdown",
            "text" => $text,
        ]);
    }

    public function editMessage(UserBan $ban, string $newText){
        Request::editMessageText([
            "chat_id" => $ban->report_chat_id,
            "message_id" => $ban->report_message_id,
            "text" => $newText,
            "reply_markup" => null,
            "parse_mode" => "markdown"
        ]);
    }
}