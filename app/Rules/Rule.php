<?php

namespace App\Rules;

use App\Models\Message;
use App\Rules\Actions\Action;
use App\Rules\Conditions\Condition;
use Illuminate\Support\Collection;
use Longman\TelegramBot\Entities\MessageEntity;

class Rule
{
    /** @var Collection|Condition[] */
    public $conditions;

    /** @var Collection|Action[] */
    public $actions;

    private $conditionExtraData = [];

    /**
     * Rule constructor.
     *
     * @param Condition[]|Collection $conditions
     * @param Action[]|Collection    $actions
     */
    public function __construct(array $conditions, array $actions)
    {
        $this->conditions = collect($conditions);
        $this->actions = collect($actions);
    }

    /**
     * @param Condition[]|Collection $conditions
     *
     * @return Rule
     */
    public function setConditions(Collection $conditions): Rule
    {
        if ($conditions->isEmpty()) {
            throw new \InvalidArgumentException();
        }
        $this->conditions = $conditions;

        return $this;
    }

    /**
     * @param Action $action
     *
     * @return Rule
     */
    public function setActions(Collection $actions): Rule
    {
        $this->actions = $actions;

        return $this;
    }

    /**
     * @param Message                    $message
     * @param Collection|MessageEntity[] $entities
     */
    public function process(Message $message, Collection $entities)
    {
        $hasFalseCondition = $this->conditions->map(function (Condition $condition) use ($message, $entities) {
            $status = $condition->isFullfilled($message, $entities);
            logger()->debug(sprintf('Condition %s status = %b', get_class($condition), $status));

            return $status;
        })->contains(false);
        if (!$hasFalseCondition) {
            $lastActionResult = true;
            foreach ($this->actions as $action) {
                if ($lastActionResult) {
                    $lastActionResult = $action->perform($message, $this);
                    logger()->debug(sprintf('Action %s status = %b', get_class($action), $lastActionResult));
                }
            }
        }
    }

    public function getDescription()
    {
        $result = '*If:* ';
        $result .= $this->getConditionDescriptions();
        $result .= "*then:*\n";
        for ($i = 0; $i < $this->actions->count(); ++$i) {
            $result .= $this->actions[$i]->getDescription();
            if ($i !== $this->actions->count() - 1) {
                $result .= ' *and* ';
            }
        }

        return $result;
    }

    public function getConditionDescriptions()
    {
        $result = '';
        for ($i = 0; $i < $this->conditions->count(); ++$i) {
            $result .= $this->conditions[$i]->getDescription();
            if ($i !== $this->conditions->count() - 1) {
                $result .= ' *and* ';
            }
            $result .= "\n";
        }

        return $result;
    }
}
