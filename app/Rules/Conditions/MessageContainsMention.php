<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 15.07.2018
 * Time: 19:56.
 */

namespace App\Rules\Conditions;

use App\Models\Message;
use Illuminate\Support\Collection;
use Longman\TelegramBot\Entities\MessageEntity;

class MessageContainsMention implements Condition
{
    public function isFullfilled(Message $message, Collection $entities)
    {
        /** @var MessageEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getType() === 'mention') {
                return true;
            }
        }

        return false;
    }

    public function getDescription(): string
    {
        return 'Message contains mention';
    }
}
