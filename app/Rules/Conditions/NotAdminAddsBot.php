<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 20.08.2018
 * Time: 14:39.
 */

namespace App\Rules\Conditions;

use App\ChatInfoService;
use App\Models\Message;
use App\Models\User;
use Illuminate\Support\Collection;

class NotAdminAddsBot implements Condition
{
    public function isFullfilled(Message $message, Collection $entities)
    {
        $newMembers = explode(',', $message->new_chat_members);
        if (\count($newMembers) === 0) {
            return false;
        }

        if (app(ChatInfoService::class)->isChatAdministrator($message->chat, $message->user)) {
            return false;
        }

        $botNames = [];

        foreach ($newMembers as $member) {
            $user = User::find($member);
            if ($user !== null && $user->is_bot) {
                $botNames[] = $user->username;
            }
        }

        return \count($botNames) > 0;
    }

    public function getDescription(): string
    {
        return 'Not an admin added a bot';
    }
}
