<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 15.07.2018
 * Time: 19:12.
 */

namespace App\Rules\Conditions;

use App\Models\Message;
use Illuminate\Support\Collection;

interface Condition
{
    public function isFullfilled(Message $message, Collection $entities);

    public function getDescription(): string;
}
