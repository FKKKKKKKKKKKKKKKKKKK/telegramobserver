<?php

namespace App\Rules\Conditions;

use App\Models\Message;
use Illuminate\Support\Collection;

class ContainsRTL implements Condition
{
    public function isFullfilled(Message $message, Collection $entities)
    {
        $rtlChar = '/[\x{0590}-\x{083F}]|[\x{08A0}-\x{08FF}]|[\x{FB1D}-\x{FDFF}]|[\x{FE70}-\x{FEFF}]/u';

        return preg_match($rtlChar, $message->text) !== 0;
    }

    public function getDescription(): string
    {
        return 'contains RTL characters';
    }
}
