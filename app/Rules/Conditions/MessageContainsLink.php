<?php

namespace App\Rules\Conditions;

use App\Models\Message;
use App\Models\WebDomainInfo;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Longman\TelegramBot\Entities\MessageEntity;

class MessageContainsLink implements Condition
{
    const SOURCE_BLACKLIST = 1;
    const SOURCE_WHITELIST = 2;
    const SOURCE_GREY_ZONE = 4;
    const SOURCE_ALL = 7;

    private $sources;

    /**
     * MessageContainsLink constructor.
     *
     * @param $sources
     */
    public function __construct(int $sources = MessageContainsLink::SOURCE_ALL)
    {
        $this->sources = $sources;
    }

    public function isFullfilled(Message $message, Collection $entities)
    {
        /** @var MessageEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getType() === 'url') {
                $url = Str::substr($message->text, $entity->getOffset(), $entity->getLength());
                $parsed = parse_url($url, PHP_URL_HOST) ?:
                    explode('/', parse_url($url, PHP_URL_PATH))[0];
                $info = WebDomainInfo::whereDomain($parsed)->first();
                if (($parsed === null || $info === null) && $this->sources & static::SOURCE_GREY_ZONE) {
                    return true;
                }
                if ($info !== null) {
                    if ($info->type === 'white' && ($this->sources & static::SOURCE_WHITELIST)) {
                        return true;
                    }
                    if ($info->type === 'black' && ($this->sources & static::SOURCE_BLACKLIST)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function getDescription(): string
    {
        $result = 'contains link. Link types: ';
        if ($this->sources === static::SOURCE_ALL) {
            return $result.'all';
        }
        if ($this->sources & static::SOURCE_WHITELIST) {
            $result .= ' white ';
        }
        if ($this->sources & static::SOURCE_BLACKLIST) {
            $result .= ' black ';
        }

        if ($this->sources & static::SOURCE_GREY_ZONE) {
            $result .= ' grey ';
        }

        return $result;
    }
}
