<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 15.07.2018
 * Time: 19:24.
 */

namespace App\Rules\Conditions;

use App\Models\Message;
use DB;
use Illuminate\Support\Collection;

class FirstNMessages implements Condition
{
    /** @var int */
    private $n;

    /**
     * FirstNMessages constructor.
     *
     * @param int $n
     */
    public function __construct(int $n)
    {
        $this->n = $n;
    }

    public function isFullfilled(Message $message, Collection $entities)
    {
        if (data_get(DB::table('user_chat')->where([
                'chat_id' => $message->chat_id,
                'user_id' => $message->user_id,
            ])->first(), 'date') === null) {
            return false;
        }

        $count = Message::query()
            ->where('chat_id', $message->chat_id)
            ->where('user_id', $message->user_id)
            ->where('id', '<=', $message->id)
            ->count();

        printf("%s \n", $count);

        return $count <= $this->n;
    }

    public function getDescription(): string
    {
        return sprintf('is among first %s message(s)', $this->n);
    }

    /**
     * @param int $n
     *
     * @return FirstNMessages
     */
    public function setN(int $n): FirstNMessages
    {
        $this->n = $n;

        return $this;
    }
}
