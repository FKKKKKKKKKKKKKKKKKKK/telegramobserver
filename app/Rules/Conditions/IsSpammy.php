<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 21.07.2018
 * Time: 17:43.
 */

namespace App\Rules\Conditions;

use App\Models\Message;
use Cache;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class IsSpammy implements Condition
{
    /** @var int */
    private $seconds;
    /** @var int */
    private $maxMessages;

    /**
     * IsSpammy constructor.
     *
     * @param int $seconds
     * @param int $maxMessages
     */
    public function __construct(int $seconds, int $maxMessages)
    {
        $this->seconds = $seconds;
        $this->maxMessages = $maxMessages;
    }

    public function isFullfilled(Message $message, Collection $entities)
    {
        $key = sprintf('tgb:spam:%s:%s', $message->chat_id, $message->user_id);
        if (Cache::has($key)) {
            return false;
        }
        $count = Message::whereChatId($message->chat_id)
            ->whereUserId($message->user_id)
            ->where('date', '>=', Carbon::now()->subSeconds($this->seconds))
            ->count();
        logger()->debug(static::class, [
            'count' => $count,
        ]);

        if ($count > $this->maxMessages) {
            Cache::put($key, '1', 15);

            return true;
        }

        return false;
    }

    public function getDescription(): string
    {
        return sprintf('User sends more than %s messages within %s seconds',
            $this->maxMessages,
            $this->seconds);
    }
}
