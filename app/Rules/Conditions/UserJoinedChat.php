<?php

namespace App\Rules\Conditions;

use App\Models\Message;
use Illuminate\Support\Collection;

class UserJoinedChat implements Condition
{
    public function isFullfilled(Message $message, Collection $entities)
    {
        return !empty($message->new_chat_members);
    }

    public function getDescription(): string
    {
        return 'User joins chat';
    }
}
