<?php

namespace App\Rules\Conditions;

use App\Models\Message;
use Illuminate\Support\Collection;

class IsNotPrivateMessage implements Condition
{
    public function isFullfilled(Message $message, Collection $entities)
    {
        return $message->chat->type !== 'private';
    }

    public function getDescription(): string
    {
        return 'Is not a private message';
    }
}
