<?php

namespace App\Rules\Conditions;

use App\Models\Message;
use Illuminate\Support\Collection;

class MessageIsForwarded implements Condition
{
    public function isFullfilled(Message $message, Collection $entities)
    {
        return $message->forward_from_chat !== null || $message->forward_from !== null;
    }

    public function getDescription(): string
    {
        return 'is forwarded from somewhere';
    }
}
