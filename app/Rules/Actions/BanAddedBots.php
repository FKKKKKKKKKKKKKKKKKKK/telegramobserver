<?php

namespace App\Rules\Actions;

use App\Models\Message;
use App\Models\User;
use App\Rules\Rule;
use Longman\TelegramBot\Request;

class BanAddedBots implements Action
{
    public function perform(Message $message, Rule $rule)
    {
        $newMembers = explode(',', $message->new_chat_members);

        foreach ($newMembers as $member) {
            $user = User::find($member);
            if ($user !== null && $user->is_bot) {
                Request::kickChatMember([
                    'chat_id' => $message->chat_id,
                    'user_id' => $member,
                ]);
            }
        }

        return true;
    }

    public function getDescription(): string
    {
        // TODO: Implement getDescription() method.
    }
}
