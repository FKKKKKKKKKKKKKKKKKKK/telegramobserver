<?php

namespace App\Rules\Actions;

use App\Models\Message;
use App\Rules\Rule;
use Longman\TelegramBot\Request;

class ReportBan implements Action
{
    public function perform(Message $message, Rule $rule)
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $result = Request::sendMessage([
            'chat_id' => $message->chat->moderation_chat_id ?? $message->chat_id,
            'parse_mode' => 'markdown',
            'text' => sprintf("User %s has been banned automatically from *%s*. \n *Reason:* %s",
                $message->user->getDescription(),
                $message->chat->title,
                $rule->getConditionDescriptions()),
        ]);

        return $result->isOk();
    }

    public function getDescription(): string
    {
        return 'Reports the ban';
    }
}
