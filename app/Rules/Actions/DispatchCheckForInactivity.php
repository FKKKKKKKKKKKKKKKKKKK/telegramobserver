<?php

namespace App\Rules\Actions;

use App\Jobs\CheckForInactivity;
use App\Models\Message;
use App\Models\User;
use App\Rules\Rule;

class DispatchCheckForInactivity implements Action
{
    public function perform(Message $message, Rule $rule)
    {
        $userIds = explode(',', $message->new_chat_members);
        foreach ($userIds as $userId) {
            $user = User::find($userId);
            if ($user !== null) {
                if (Message::where([
                        'chat_id' => $message->chat_id,
                        'user_id' => $message->user_id,
                    ])->count() > 50) {
                    continue;
                }
                dispatch(new CheckForInactivity(900, $user, $message->chat));
            }
        }
    }

    public function getDescription(): string
    {
        return 'Dispatches check for inactivity delayed by 15 min';
    }
}
