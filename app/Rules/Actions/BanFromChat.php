<?php

namespace App\Rules\Actions;

use App\Models\Message;
use App\Rules\Rule;
use Longman\TelegramBot\Request;

class BanFromChat implements Action
{
    public function perform(Message $message, Rule $rule)
    {
        $result = Request::kickChatMember([
            'chat_id' => $message->chat_id,
            'user_id' => $message->user_id,
        ]);

        return $result->isOk();
    }

    public function getDescription(): string
    {
        return 'Bans user from chat';
    }
}
