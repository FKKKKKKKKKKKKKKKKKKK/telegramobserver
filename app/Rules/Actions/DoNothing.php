<?php

namespace App\Rules\Actions;

use App\Models\Message;
use App\Rules\Rule;

class DoNothing implements Action
{
    public function perform(Message $message, Rule $rule)
    {
        echo 'did nothing';
    }

    public function getDescription(): string
    {
        return 'Does nothing';
    }
}
