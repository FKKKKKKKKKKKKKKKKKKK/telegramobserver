<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 15.07.2018
 * Time: 19:13.
 */

namespace App\Rules\Actions;

use App\Models\Message;
use App\Rules\Rule;

interface Action
{
    public function perform(Message $message, Rule $rule);

    public function getDescription(): string;
}
