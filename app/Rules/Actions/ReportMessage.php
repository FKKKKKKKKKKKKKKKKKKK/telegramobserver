<?php

namespace App\Rules\Actions;

use App\Models\Message;
use App\Models\UserBan;
use App\ReportService;
use App\Rules\Rule;
use Longman\TelegramBot\Entities\ServerResponse;

class ReportMessage implements Action
{
    public function perform(Message $message, Rule $rule)
    {
        if ($message->fresh()->is_reported) {
            return false;
        }
        $chat = $message->chat;
        $moderationChat = $chat->moderationChat ?? $chat;

        /** @var UserBan $ban */
        $ban = UserBan::create([
            'user_id' => $message->user_id,
            'chat_id' => $message->chat_id,
        ]);
        /** @var ServerResponse $result */
        $result = app(ReportService::class)->sendReportMessage($moderationChat,
            sprintf("Message '%s' in chat '%s' written by %s is suspicious:\n%s",
                $message->getText(50),
                $chat->title,
                $message->user->getDescription(),
                $rule->getConditionDescriptions()
            ),
            $ban
        );

        dump($result);

        if ($result->isOk()) {
            $message->is_reported = true;
            $ban->report_message_id = $result->getResult()->message_id;
            $ban->report_chat_id = $result->getResult()->getChat()->id;
            $message->save();
            $ban->save();
        }

        return $result->isOk();
    }

    public function getDescription(): string
    {
        return 'Reports the message to moderation channel';
    }
}
